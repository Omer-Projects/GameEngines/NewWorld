#pragma once

#include <GLM/glm.hpp>

namespace NewWorld::Math
{
	using Vector3 = glm::vec3;
}