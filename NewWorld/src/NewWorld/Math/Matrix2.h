#pragma once

#include <GLM/glm.hpp>

namespace NewWorld::Math
{
	using Matrix2 = glm::mat2;
}